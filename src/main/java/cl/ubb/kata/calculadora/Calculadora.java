package cl.ubb.kata.calculadora;

public class Calculadora {
	
	
	public int sumar(int a, int b){
		return(a+b);
	}

	public int multiplicar(int a, int b){
		return a*b;
	}
	
	public int dividir(int a, int b){
		return a/b;
	}
}
