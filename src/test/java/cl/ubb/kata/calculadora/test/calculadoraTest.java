package cl.ubb.kata.calculadora.test;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import cl.ubb.kata.calculadora.Calculadora;


public class calculadoraTest {
	private Calculadora calculadora;
	int resultado=0;
	
	@Before
	public void SetUp(){
		calculadora=new Calculadora();
	}

	@Test
	public void SumarUnoMasUnoIgualDos() {
		int a=1;
		int b=1;
		assertEquals(2, calculadora.sumar(a,b));
		
	}
	
	@Test
	public void SumarUnoMasDosNegativoIgualMenosUno() {
		int a=1;
		int b=-2;
		assertEquals(-1, calculadora.sumar(a,b));
		
	}
	
	@Test
	public void SumarUnoNegativoMasUnoNegativoIgualMenosDos() {
		int a=-1;
		int b=-1;
		assertEquals(-2, calculadora.sumar(a,b));
		
	}
	
	@Test
	public void MultiplicarDosPorTresIgualSeis(){
		int a=2,b=3;
		assertEquals(6, calculadora.multiplicar(a,b));
	}
	
	@Test
	public void MultiplicarMenosDosPorMenosTresIgualASeis(){
		int a=-2,b=-3;
		assertEquals(6, calculadora.multiplicar(a,b));
	}
	
	@Test
	public void DividirCuatroPorDosIgualOcho(){
		int a=4,b=2;
		assertEquals(2, calculadora.dividir(a,b));
	}
	
	@Test
	public void DividirSeisPorDosIgualTres(){
		int a=6,b=2;
		
		assertEquals(3, calculadora.dividir(a,b));
	}



}
